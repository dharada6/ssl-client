package jp.co.so_net.mv.client.http;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;

import bad.robot.http.FormParameters;
import bad.robot.http.FormUrlEncodedMessage;
import bad.robot.http.HttpDelete;
import bad.robot.http.HttpException;
import bad.robot.http.HttpGet;
import bad.robot.http.HttpRequest;
import bad.robot.http.HttpRequestVisitor;
import bad.robot.http.StringMessageContent;
import bad.robot.http.Transform;
import bad.robot.http.UnencodedStringMessage;

public class IspHttpRequestToEntity implements HttpRequestVisitor {

	private final HttpRequest message;
	private HttpEntity entity;

	public IspHttpRequestToEntity(HttpRequest message) {
		this.message = message;
	}

	public HttpEntity asHttpEntity() {

		if (message != null) {
			message.accept(this);
		}

		return entity;
	}

	@Override
	public void visit(HttpDelete message) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void visit(HttpGet message) {
		throw new UnsupportedOperationException();
	}

	public void visit(IspRequestMessage ispRequestMessage) {
		try {
			IspFormParameters content = ispRequestMessage.getContent();
			entity = new UrlEncodedFormEntity(content.transform(asApacheNameValuePair()),
			        ispRequestMessage.characterSet());
		} catch (UnsupportedEncodingException e) {
			throw new HttpException(e);
		}
	}

	@Override
	public void visit(FormUrlEncodedMessage formUrlEncodedMessage) {
		try {
			FormParameters content = formUrlEncodedMessage.getContent();
			entity = new UrlEncodedFormEntity(content.transform(asApacheNameValuePair()),
			        formUrlEncodedMessage.characterSet());
		} catch (UnsupportedEncodingException e) {
			throw new HttpException(e);
		}
	}

	@Override
	public void visit(UnencodedStringMessage unencodedStringMessage) {
		try {
			StringMessageContent content = unencodedStringMessage.getContent();
			entity = new StringEntity(content.asString(), unencodedStringMessage.characterSet());
		} catch (UnsupportedEncodingException e) {
			throw new HttpException(e);
		}
	}

	private Transform<Map.Entry<String, String>, NameValuePair> asApacheNameValuePair() {
		return new Transform<Map.Entry<String, String>, NameValuePair>() {
			@Override
			public NameValuePair call(Map.Entry<String, String> tuple) {
				return new BasicNameValuePair(tuple.getKey(), tuple.getValue());
			}
		};
	}
}
