package jp.co.so_net.mv.client.constants;

public class Const {

	public static final String UTF_8 = "UTF-8";

	/**
	 * 半角スペース
	 */
	public static String HANKAKU_SPACE = " ";

	/**
	 * ブランク.
	 */
	public static final String BLANK = "";

	/**
	 * リソースバンドル名 ("ivr-isp")
	 */
	public static final String IVR_ISP = "ivr-isp";
}
