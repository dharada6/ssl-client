package jp.co.so_net.mv.client.constants;

/**
 * 本アプリのバージョン番号を管理するクラス
 * 
 */
public final class AppVersion {

    public static final String version = "1.0.0";

}
