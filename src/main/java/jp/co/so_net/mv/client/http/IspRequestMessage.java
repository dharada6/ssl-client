package jp.co.so_net.mv.client.http;

import static bad.robot.http.CharacterSet.defaultCharacterSet;
import static bad.robot.http.EmptyHeaders.emptyHeaders;
import static java.lang.String.format;
import static jp.co.so_net.mv.client.util.UtilFactory.getResouceBundleUtil;
import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;

import java.util.ResourceBundle;

import bad.robot.http.CharacterSet;
import bad.robot.http.Headers;
import bad.robot.http.HttpPost;
import bad.robot.http.HttpRequestVisitor;

public class IspRequestMessage implements HttpPost {

    private final Headers headers;
    private final IspFormParameters content;

    @SuppressWarnings("unused")
    private final CharacterSet characterSet;

    public IspRequestMessage(IspFormParameters content) {
        this(content, emptyHeaders());
    }

    public IspRequestMessage(IspFormParameters content, Headers headers) {
        this(content, headers, defaultCharacterSet);
    }

    public IspRequestMessage(IspFormParameters content,
            CharacterSet characterSet) {
        this(content, emptyHeaders(), characterSet);
    }

    public IspRequestMessage(IspFormParameters content, Headers headers,
            CharacterSet characterSet) {
        this.headers = headers;
        this.content = content;
        this.characterSet = characterSet;
    }

    @Override
    public Headers getHeaders() {
        // Content-Type and Content-Length are already set in UrlParameters (at
        // least for Apache)
        return headers;
    }

    @Override
    public IspFormParameters getContent() {
        return content;
    }

    @Override
    public void accept(HttpRequestVisitor visitor) {

        if (visitor instanceof IspHttpRequestToEntity) {

            IspHttpRequestToEntity.class.cast(visitor).visit(this);

        } else {

            throw new RuntimeException("(想定外エラー) 不正なHttpRequestVisitorです。"
                    + visitor.getClass().getName());

        }

    }

    @Override
    public int hashCode() {
        return reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object that) {
        return reflectionEquals(this, that);
    }

    @Override
    public String toString() {
        return format("%s{content='%s', headers='%s'characterSet='%s'}", this
                .getClass().getSimpleName(), content, headers, characterSet());
    }

    /**
     * So-netISP側は、Shift_JISのリクエストを要求している。（I/F仕様）
     */
    public String characterSet() {
        return ispBundle().getString("encoding");
    }

    ResourceBundle ispBundle() {
        return getResouceBundleUtil().getIvrIspBundle();
    }

}
