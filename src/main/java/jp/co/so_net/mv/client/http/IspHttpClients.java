package jp.co.so_net.mv.client.http;

import java.net.URL;
import java.util.ResourceBundle;

import bad.robot.http.CommonHttpClient;
import bad.robot.http.Headers;
import bad.robot.http.HttpException;
import bad.robot.http.HttpPost;
import bad.robot.http.HttpPut;
import bad.robot.http.HttpResponse;
import bad.robot.http.apache.ApacheAuthenticationSchemeHttpContextBuilder;
import bad.robot.http.apache.ApacheHttpClientBuilder;
import bad.robot.http.apache.IspApacheHttpClient;
import bad.robot.http.apache.Ssl;
import bad.robot.http.configuration.AuthorisationCredentials;
import bad.robot.http.configuration.AutomaticRedirectHandling;
import bad.robot.http.configuration.HttpTimeout;
import bad.robot.http.configuration.Proxy;
import static bad.robot.http.apache.ApacheAuthenticationSchemeHttpContextBuilder.*;
import static bad.robot.http.apache.ApacheHttpClientBuilder.*;
import static jp.co.so_net.mv.client.util.UtilFactory.*;

/**
 * 
 */
public class IspHttpClients {

	public static CommonHttpClient anApacheClient() {
		
		setHttpsProtocols();
		setSslTrustStore();
		setSslKeyStore();

		return new IspApacheCommonHttpClient();
	}

	static void setHttpsProtocols() {
		System.setProperty("https.protocols", "TLSv1");
	}

	/**
	 * クライアント認証用の設定
	 */
	private static void setSslKeyStore() {

		// pkcs12形式をデフォルトとする。
		System.setProperty("javax.net.ssl.keyStoreType", keyStoreType());
		System.setProperty("javax.net.ssl.keyStore", keyStore());
		System.setProperty("javax.net.ssl.keyStorePassword", keyStorePassword());

	}

	private static String keyStorePassword() {
		return ispBundle().getString("javax.net.ssl.keyStorePassword");
	}

	private static String keyStore() {
		return ispBundle().getString("javax.net.ssl.keyStore");
	}

	private static String keyStoreType() {
		return ispBundle().getString("javax.net.ssl.keyStoreType");
	}

	/**
	 * サーバ認証用の設定
	 */
	static void setSslTrustStore() {

		System.setProperty("javax.net.ssl.trustStore", trustStore());
		System.setProperty("javax.net.ssl.trustStorePassword", trustStorePassword());

		System.setProperty("javax.net.debug", sslDebug());

	}

	static String sslDebug() {

		try {
			return ispBundle().getString("javax.net.debug");
		} catch (Exception e) {
			return "";
		}
	}

	static ResourceBundle ispBundle() {
		return getResouceBundleUtil().getIvrIspBundle();
	}

	static String trustStore() {
		return ispBundle().getString("javax.net.ssl.trustStore");
	}

	static String trustStorePassword() {
		return ispBundle().getString("javax.net.ssl.trustStorePassword");
	}

	private static class IspApacheCommonHttpClient implements CommonHttpClient {

		private final ApacheHttpClientBuilder apache = anApacheClientWithShortTimeout();
		private final ApacheAuthenticationSchemeHttpContextBuilder authenticationSchemes = anApacheBasicAuthScheme();

		private IspApacheHttpClient httpClient;

		@Override
		public CommonHttpClient with(AuthorisationCredentials credentials) {
			credentials.applyTo(apache);
			credentials.applyTo(authenticationSchemes);
			return this;
		}

		@Override
		public CommonHttpClient with(HttpTimeout timeout) {
			apache.with(timeout);
			return this;
		}

		@Override
		public CommonHttpClient withoutSsl() {
			apache.with(Ssl.disabled);
			return this;
		}

		@Override
		public CommonHttpClient withTrustingSsl() {
			apache.with(Ssl.naive);
			return this;
		}

		@Override
		public CommonHttpClient with(AutomaticRedirectHandling handleRedirects) {
			apache.with(handleRedirects);
			return this;
		}

		@Override
		public CommonHttpClient with(Proxy proxy) {
			apache.with(proxy);
			return this;
		}

		@Override
		public HttpResponse get(URL url) throws HttpException {
			initialiseHttpClient();
			return httpClient.get(url);
		}

		@Override
		public HttpResponse get(URL url, Headers headers) throws HttpException {
			initialiseHttpClient();
			return httpClient.get(url, headers);
		}

		@Override
		public HttpResponse post(URL url, HttpPost message) throws HttpException {
			initialiseHttpClient();
			return httpClient.post(url, message);
		}

		@Override
		public HttpResponse put(URL url, HttpPut message) throws HttpException {
			initialiseHttpClient();
			return httpClient.put(url, message);
		}

		@Override
		public HttpResponse delete(URL url) throws HttpException {
			initialiseHttpClient();
			return httpClient.delete(url);
		}

		@Override
		public HttpResponse options(URL url) throws HttpException {
			initialiseHttpClient();
			return httpClient.options(url);
		}

		@Override
		public void shutdown() {
			initialiseHttpClient();
			httpClient.shutdown();
		}

		private void initialiseHttpClient() {
			if (httpClient == null) {
				httpClient = new IspApacheHttpClient(apache, authenticationSchemes);
			}
		}
	}

}
