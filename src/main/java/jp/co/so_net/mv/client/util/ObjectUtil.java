package jp.co.so_net.mv.client.util;

/**
 * オブジェクトユーティリティ
 */
public class ObjectUtil {

    /**
     * 引数がNullかどうかチェックする。
     * 
     * @param obj
     * @return
     */
    public static boolean isNull(Object obj) {
        return obj == null;
    }

    /**
     * 
     * @param obj
     * @return
     */
    public static boolean isNotNull(Object obj) {
        return !isNull(obj);
    }
}
