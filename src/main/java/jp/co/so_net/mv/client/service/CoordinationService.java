package jp.co.so_net.mv.client.service;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import jp.co.so_net.mv.client.http.IspRequestMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bad.robot.http.HttpException;
import bad.robot.http.HttpResponse;

import com.google.code.tempusfugit.temporal.Duration;

import static bad.robot.http.configuration.HttpTimeout.*;
import static jp.co.so_net.mv.client.http.IspHttpClients.*;
import static jp.co.so_net.mv.client.util.UtilFactory.*;

/**
 * ISP連携を担うサービスクラス
 * 
 */
public class CoordinationService extends AbstractService {

	private static final Logger log = LoggerFactory.getLogger(CoordinationService.class);

	public CoordinationService() {
	}

	/**
	 * 設定ファイルに定義されたURLを呼び出す。パラメータはなし。
	 * <p>
	 * </p>
	 * 
	 * @param request
	 * @return response content as String
	 */
	public String post() {

		HttpResponse response = null;

		// retry loop
		for (int counter = 0; counter <= retry(); counter++) {

			try {

				// 　https/http通信 with POST (通信疎通確認の為、送信データなし=null）
				response = anApacheClient().with(httpTimeout(duration())).post(url(), null);

			} catch (MalformedURLException e) {

				// url不正。リトライなし。
				throw new RuntimeException(e);

			} catch (HttpException e) {

				if (counter < retry()) {
					// リトライ上限まで、リトライ
					continue;
				} else {
					throw new RuntimeException(e);
				}

			}

		}

		return getContent(response);

	}

	int retry() {
		return Integer.parseInt(retryCount());
	}

	String retryCount() {
		return getResouceBundleUtil().getIvrIspBundle().getString("retry");
	}

	/**
	 * return null
	 * 
	 * @param values
	 * @return
	 */
	IspRequestMessage requestMessage(List<String> values) {
		return null;
	}

	String[] toArray(List<String> values) {
		return values.toArray(new String[0]);
	}

	Duration duration() {
		return Duration.seconds(timeout());
	}

	URL url() throws MalformedURLException {
		return new URL(ispBundle().getString("cardAurhoriBasic.url"));
	}

	ResourceBundle ispBundle() {
		return getResouceBundleUtil().getIvrIspBundle();
	}

	long timeout() {
		return Long.parseLong(ispBundle().getString("timeout"));
	}

	private String getContent(HttpResponse response) {
		return infoLog(response);
	}

	String infoLog(HttpResponse response) {

		log.info("HttpResponse#getStatusCode()=" + response.getStatusCode());
		log.info("HttpResponse#getHeaders()#toString()=" + response.getHeaders().toString());

		String content = response.getContent().asString();
		log.info("HttpResponse#getContent()#asString()=" + content);

		return content;
	}

	/**
	 * 改行コードは、LF固定
	 * 
	 * @return
	 */
	String LF() {
		return "\n";
	}

}
