/*
 * Copyright 2014 So-net Corporation
 */
package jp.co.so_net.mv.client.service;

import jp.co.so_net.mv.client.constants.Const;
import jp.co.so_net.mv.client.util.ObjectUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * サービスの抽象クラスです。
 * 
 */
public abstract class AbstractService {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(AbstractService.class);

	public AbstractService() {
	}

	protected String ivrIspBundleName() {
		return Const.IVR_ISP;
	}

	protected boolean isNull(Object obj) {
		return ObjectUtil.isNull(obj);
	}

	protected boolean isNotNull(Object obj) {
		return ObjectUtil.isNotNull(obj);
	}

}