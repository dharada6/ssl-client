package jp.co.so_net.mv.client;

import jp.co.so_net.mv.client.service.CoordinationService;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is process start point which has main method.
 */
public class App {

	private static final Logger log = LoggerFactory.getLogger(App.class);

	public static void main(String[] args) {

		try {

			log.info("jp.co.so_net.mv.client.App#main [start]");
			log.info("Response content is " + new CoordinationService().post());

		} catch (Throwable th) {

			log.error(ExceptionUtils.getStackTrace(th));

			throw th;

		} finally {
			log.info("jp.co.so_net.mv.client.App#main [end]");
		}

	}
}
