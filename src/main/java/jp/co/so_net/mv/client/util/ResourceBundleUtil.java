package jp.co.so_net.mv.client.util;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;

import static jp.co.so_net.mv.client.constants.Const.*;
import static jp.co.so_net.mv.client.util.UtilFactory.*;

/**
 * リソースバンドル用のユーティリティ
 * 
 */
public class ResourceBundleUtil {

	ResourceBundleUtil() {
	}

	/**
	 * 
	 * デフォルトロケールを使用する。
	 * 
	 * @param name
	 *            バンドル名
	 * @return　ResourceBundle
	 */
	public ResourceBundle getBundle(String name) {
		return org.seasar.framework.util.ResourceBundleUtil.getBundle(name, null);
	}

	/**
	 * <b>キャッシュしない</b>
	 */
	public ResourceBundle getNoCacheBundle(String name) {

		Control control = new ResourceBundle.Control() {
			@Override
			public long getTimeToLive(String baseName, Locale locale) {
				return TTL_DONT_CACHE;
			}
		};

		return ResourceBundle.getBundle(name, Locale.getDefault(), control);

	}

	/**
	 * キャッシュあり。（30分間）
	 * 
	 * 
	 * @param name
	 * @return
	 */
	public ResourceBundle getHalfHourCacheBundle(String name) {

		Control control = new ResourceBundle.Control() {
			@Override
			public long getTimeToLive(String baseName, Locale locale) {
				return 30 * 60000; // 1800秒 （30分）
			}
		};

		return ResourceBundle.getBundle(name, Locale.getDefault(), control);

	}

	/**
	 * キャッシュあり。（1時間）
	 * 
	 * @param name
	 * @return
	 */
	public ResourceBundle getOneHourCacheBundle(String name) {

		Control control = new ResourceBundle.Control() {
			@Override
			public long getTimeToLive(String baseName, Locale locale) {
				return 60 * 60000; // 3600秒 （一時間）
			}
		};

		return ResourceBundle.getBundle(name, Locale.getDefault(), control);

	}

	public ResourceBundle getIvrIspBundle() {
		return getResouceBundleUtil().getOneHourCacheBundle(IVR_ISP);
	}

}
