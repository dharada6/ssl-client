package jp.co.so_net.mv.client.http;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import bad.robot.http.HttpException;
import bad.robot.http.MessageContent;
import bad.robot.http.Transform;
import static jp.co.so_net.mv.client.util.UtilFactory.*;
import static org.apache.commons.lang3.builder.EqualsBuilder.*;
import static org.apache.commons.lang3.builder.HashCodeBuilder.*;

/**
 * ISP連携用 POSTパラメータを表現するクラス　
 * 
 * <P>
 * URLEncode with "Shift_JIS"
 * </P>
 * 
 */
public class IspFormParameters implements MessageContent {

    private final Map<String, String> parameters = new HashMap<String, String>();

    public static IspFormParameters params(String... values) {
        return new IspFormParameters(values);
    }

    private IspFormParameters(String... values) {
        if (values.length % 2 != 0) {
            throw new IllegalArgumentException(
                    "should be a even number of arguments, received"
                            + values.length);
        }
        toMap(values);
    }

    private void toMap(String[] values) {
        for (int i = 0; i < values.length; i += 2) {
            parameters.put(values[i], values[i + 1]);
        }
    }

    public <T> List<T> transform(
            Transform<Map.Entry<String, String>, T> transform) {

        List<T> pairs = new ArrayList<T>();
        for (Map.Entry<String, String> parameter : parameters.entrySet()) {
            pairs.add(transform.call(parameter));
        }
        return pairs;
    }

    public String asString() {
        StringBuilder builder = new StringBuilder();
        Iterator<Map.Entry<String, String>> iterator = parameters.entrySet()
                .iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> parameter = iterator.next();
            builder.append(encode(parameter.getKey())).append("=")
                    .append(encode(parameter.getValue()));
            if (iterator.hasNext()) {
                builder.append("&");
            }
        }
        return builder.toString();
    }

    /**
     * So-netISP側がShift_JISを要求している。
     */
    private String encode(String value) {
        try {
            return URLEncoder.encode(value, encoding());
        } catch (UnsupportedEncodingException e) {
            throw new HttpException(e);
        }
    }

    /**
     * 設定ファイルから、ISP連携用のエンコーディングを取得
     * 
     * @return
     */
    String encoding() {
        // Shift_JIS
        return ispBundle().getString("encoding");
    }

    ResourceBundle ispBundle() {
        return getResouceBundleUtil().getIvrIspBundle();
    }

    @Override
    public int hashCode() {
        return reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object that) {
        return reflectionEquals(this, that);
    }

    @Override
    public String toString() {
        return asString();
    }

}
