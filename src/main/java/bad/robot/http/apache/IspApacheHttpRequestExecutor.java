package bad.robot.http.apache;

import java.util.concurrent.Callable;

import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.protocol.HttpContext;

import bad.robot.http.HttpResponse;

/**
 * 
 * {@link ApacheHttpRequestExecutor}が、package
 * protectedである為、やむを得ず、同じパッケージ構成として、実装する。
 * <p>
 * http://baddotrobot.com/blog/2012/06/10/http-simple/
 * 
 */
public class IspApacheHttpRequestExecutor implements Callable<HttpResponse> {

    private final org.apache.http.client.HttpClient client;
    private final HttpContext localContext;
    private final HttpUriRequest request;

    public IspApacheHttpRequestExecutor(
            org.apache.http.client.HttpClient client, HttpContext localContext,
            HttpUriRequest request) {
        this.request = request;
        this.client = client;
        this.localContext = localContext;
    }

    public HttpResponse call() throws Exception {
        return client.execute(request, new HttpResponseHandler(request,
                new ToStringConsumer()), localContext);
    }

}
