package bad.robot.http.apache;

import java.net.URL;
import java.util.concurrent.Callable;

import jp.co.so_net.mv.client.http.IspHttpRequestToEntity;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.protocol.HttpContext;

import bad.robot.http.Builder;
import bad.robot.http.Executor;
import bad.robot.http.Header;
import bad.robot.http.Headers;
import bad.robot.http.HttpException;
import bad.robot.http.HttpResponse;
import static bad.robot.http.EmptyHeaders.*;
import static bad.robot.http.apache.Coercions.*;

/**
 * simple-httpライブラリを拡張
 * 
 * {@link ApacheHttpClient}が、package protectedである為、やむを得ず、同じパッケージ構成として、実装する。
 * <p>
 * http://baddotrobot.com/blog/2012/06/10/http-simple/
 * 
 */
public class IspApacheHttpClient implements bad.robot.http.HttpClient {

	private final org.apache.http.client.HttpClient client;
	private final HttpContext localContext;
	private final Executor<HttpException> executor;

	public IspApacheHttpClient(Builder<HttpClient> clientBuilder,
	        Builder<HttpContext> localContextBuilder) {

		this.client = clientBuilder.build();
		this.localContext = localContextBuilder.build();
		this.executor = new ApacheExceptionWrappingExecutor();
	}

	@Override
	public HttpResponse post(URL url, bad.robot.http.HttpPost message) throws HttpException {

		HttpPost post = new HttpPost(url.toExternalForm());
		for (Header header : message.getHeaders()) {
			post.addHeader(header.name(), header.value());
		}

		// IspHttpRequestToEntityをインスタンス化
		post.setEntity(new IspHttpRequestToEntity(message).asHttpEntity());

		removeExpect100ContinueHeader(post);

		return execute(post);
	}

	/**
	 * HTTP Headerから、Expect=100-continueを削除する。
	 * 
	 * <p>
	 * <b>古いHTTPサーバの場合、Expectを理解できないケースがある。</b>
	 * </p>
	 * 
	 * @param post
	 */
	@SuppressWarnings("deprecation")
	void removeExpect100ContinueHeader(HttpPost post) {

		post.getParams().setBooleanParameter(
		        org.apache.http.params.CoreProtocolPNames.USE_EXPECT_CONTINUE, false);

	}

	private HttpResponse execute(HttpUriRequest request) {
		return executor.submit(http(request));
	}

	private Callable<HttpResponse> http(HttpUriRequest request) {
		return new IspApacheHttpRequestExecutor(client, localContext, request);
	}

	@Override
	public HttpResponse get(URL url, Headers headers) throws HttpException {
		HttpGet get = new HttpGet(url.toExternalForm());
		get.setHeaders(asApacheBasicHeader(headers));
		return execute(get);
	}

	@Override
	public HttpResponse get(URL url) throws HttpException {
		return get(url, emptyHeaders());
	}

	@Override
	public HttpResponse put(URL url, bad.robot.http.HttpPut message) {
		HttpPut put = new HttpPut(url.toExternalForm());
		for (Header header : message.getHeaders()) {
			put.addHeader(header.name(), header.value());
		}
		put.setEntity(new HttpRequestToEntity(message).asHttpEntity());
		return execute(put);
	}

	@Override
	public HttpResponse delete(URL url) throws HttpException {
		return execute(new HttpDelete(url.toExternalForm()));
	}

	@Override
	public HttpResponse options(URL url) throws HttpException {
		return execute(new HttpOptions(url.toExternalForm()));
	}

	@Override
	public void shutdown() {
		client.getConnectionManager().shutdown();
	}

}
